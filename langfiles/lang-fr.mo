��          �            x     y     �     �     �  &   �     �  H   �     )     0     5     ;     K     i     ~     �  D  �     �     �          !  3   )     ]  U   e     �     �     �     �  &   �     
     &     2                        	                                         
              Article topics list Create comments Edit articles Filter Filter topics accessibles by this user Groups Groups are displayed for this right, user filter is tested on this right Manage Name Right Submit articles Topic list with access rights Topics access rights User View articles Project-Id-Version: whataccess
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-29 17:34+0100
PO-Revision-Date: 2015-05-29 17:35+0100
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: whataccess_translate;whataccess_translate:1,2
X-Generator: Poedit 1.7.6
X-Poedit-SearchPath-0: programs
 Liste des thèmes d'articles Créer des commentaires Modifier les articles Filtrer Filtrer les thèmes accessible pour cet utilisateur Groupes Les groupes sont affichés pour ce droit, les utilisateurs sont filtrés sur ce droit Gérer Nom Droit Soumettre des articles Liste des thèmes avec droits d'accès Droits d'accès des thèmes Utilisateur Voir les articles 