;<?php /*

[general]
name							="whataccess"
version							="0.3"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="List topics and associated access rights"
description.fr					="Liste les thèmes d'articles et leurs droits associés"
delete							="1"
ov_version						="8.2.0"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
mysql_character_set_database	="latin1,utf8"
icon							="icon.png"
tags                            ="library"

[addons]

jquery						=">=1.7.1.5"
widgets						=">=1.0.34"
LibTranslate				=">=1.12.0rc3.01"

;*/