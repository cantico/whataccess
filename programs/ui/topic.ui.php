<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'admin/acl.php';





class whataccess_TopicList {
    
    /**
     * @var string
     */
    private $right = BAB_TOPICSVIEW_GROUPS_TBL;
    
    /**
     * @var int
     */
    private $user;
	

    public function __construct(Array $filter = null)
    {
        $this->groups = array(BAB_REGISTERED_GROUP, BAB_UNREGISTERED_GROUP);
        

        
        if ($filter['user']) {
            $this->user = (int) $filter['user'];
        }
        
        if ($filter['right']) {
            $this->right = $filter['right'];
        }
    }
    
    
    private function getRights()
    {
        return array(
            BAB_TOPICSVIEW_GROUPS_TBL => whataccess_translate('View articles'),
            BAB_TOPICSSUB_GROUPS_TBL => whataccess_translate('Submit articles'),
            BAB_TOPICSMOD_GROUPS_TBL => whataccess_translate('Edit articles'),
            BAB_TOPICSMAN_GROUPS_TBL => whataccess_translate('Manage'),
            BAB_TOPICSCOM_GROUPS_TBL => whataccess_translate('Create comments')
        );
    }
    
    
    public function getFilterForm()
    {
        $W = bab_Widgets();
        $form = $W->Form(null, 
            $W->FlowLayout()
                ->setHorizontalSpacing(4, 'em')
                ->setVerticalAlign('bottom')
        );
        
        $form->addClass(Func_Icons::ICON_LEFT_16);
        $form->addClass('widget-bordered');
        $form->setName('filter');
        
        $form->setHiddenValue('tg', bab_rp('tg'));
        
        
        $form->addItem(
            $W->LabelledWidget(
                whataccess_translate('Right'),
                $W->Select()->setOptions($this->getRights()),
                'right',
                whataccess_translate('Groups are displayed for this right, user filter is tested on this right')
            )
        );
        
        $form->addItem(
            $W->LabelledWidget(
                whataccess_translate('User'),
                $W->UserPicker(),
                'user',
                whataccess_translate('Filter topics accessibles by this user')
            )
        );
        
        $form->addItem(
            $W->SubmitButton()
                ->setAction(whataccess_Controller()->Topic()->displaylist())
                ->setLabel(whataccess_translate('Filter'))
        );
        
        
        $form->setValues(array('filter' => array(
            'user' => $this->user,
            'right' => $this->right
        )));
        
        return $form;
    }
    
    /**
     * get topics according to filters
     * @return array
     */
    protected function getTopics()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/topincl.php';
        $topics = array();
        
        global $babDB;
        
        $query = 'SELECT * FROM bab_topics';
        
        
        
        if (isset($this->user)) {
            $objects = bab_getAccessibleObjects($this->right, $this->user);
            $query .= ' WHERE id IN('.$babDB->quote($objects).')';
        }
        
        $res = $babDB->db_query($query);
        
        while ($topic = $babDB->db_fetch_assoc($res)) {
            
            $topic['hierarchy'] = bab_unhtmlentities(viewCategoriesHierarchy_txt($topic['id'])); // html
            
            
            $topics[] = $topic;
        }
        
        // sort
        bab_Sort::asort($topics, 'hierarchy', bab_Sort::CASE_INSENSITIVE);
        return $topics;
    }
    
    
    protected function getTopicWidget($topic)
    {
        $hierarchy = explode('>', $topic['hierarchy']);
        unset($hierarchy[0]);
        unset($hierarchy[count($hierarchy)]);
        
        $position = implode('>', $hierarchy);
        
        $W = bab_Widgets(); 
        return $W->VBoxItems(
            $W->Label($topic['category'])->addClass('widget-strong'),
            $W->Label($position)
        );
    }
    
    
    protected function getGroupsWidget($topic)
    {
        $W = bab_Widgets();
        $value = aclGetRightsString($this->right, $topic['id']);
        return $W->Html($W->Acl()->setValue($value)->getDisplayValueVBox($W->HtmlCanvas()));
    }
    

	public function getTableView()
	{
		$W = bab_Widgets();
		$table = $W->BabTableView();

		$row = 0;
		$table->addHeadRow($row);
		$table->addItem($W->Label(whataccess_translate('Name')), $row, 0);
		$table->addItem($W->Label(whataccess_translate('Groups')), $row, 1);
		$row++;

		foreach ($this->getTopics() as $topic) {
			$table->addItem($this->getTopicWidget($topic), $row, 0);
			$table->addItem($this->getGroupsWidget($topic), $row, 1);

			$row++;
		}
	
		return $table;
	}
	
	
	
}
