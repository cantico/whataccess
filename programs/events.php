<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';

function whataccess_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{
	bab_functionality::includeOriginal('Icons');
	
	if (bab_isUserAdministrator()) {
		$position = array('root', 'DGAll', 'babAdmin', 'babAdminSection');
		
		$item = $event->createItem('whataccessList');
		
		$item->setLabel(whataccess_translate('Topics access rights'));
		$item->setDescription(whataccess_translate('Topic list with access rights'));
		$item->setLink(whataccess_Controller()->Topic()->displaylist()->url());
		$item->addIconClassname(Func_Icons::OBJECTS_PUBLICATION_TOPIC);
		$item->setPosition($position);
		
		$event->addFunction($item);
	}
}


